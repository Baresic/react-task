import React from 'react';
import {render} from 'react-dom';
import {Provider} from 'react-redux';
import App from './App';
import configureStore from './store/configureStore';
import './index.css';
import registerServiceWorker from './registerServiceWorker';

const store = configureStore();

const target = document.querySelector('#root')

render(
  <Provider store={store}>
        <App />
  </Provider>,
  target
)
registerServiceWorker();


