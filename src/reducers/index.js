import { combineReducers } from 'redux';
import subscriberReducer from './subscriberReducer';

const reducers = combineReducers({
    subscriberReducer,
});

export default reducers;
