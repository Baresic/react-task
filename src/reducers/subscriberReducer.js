import actionTypes from '../actions/actionTypes';

const initialState = {
    subscriber: [],
    showing: false,
};

const subscriberReducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_NEW_SUBSCRIBER: {
            return {
                ...state,
                subscriber: [
                    ...state.subscriber,
                    action.payload
                ],
            }
        }
        case actionTypes.REMOVE_SUBSCRIBER: {
            return {
                subscriber: state.subscriber.filter( (item, index) => index !== action.payload),
            }
        }
        case actionTypes.SHOW_BUTTON_VIEW: {
            return {
                ...state,
                showing: action.payload,
            }
        }
        case actionTypes.SHOW_INPUT_VIEW: {
            return {
                ...state,
                showing: action.payload,
            }
        }
        default:
            return state;
    }
};

export default subscriberReducer;
