import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Input extends Component {
    static propTypes = {
        name: PropTypes.string,
        disabled: PropTypes.bool,
        className: PropTypes.string,
        classNameWrap: PropTypes.string,
        label: PropTypes.string,
        type: PropTypes.string,
        placeholder: PropTypes.string,
        onInputChange: PropTypes.func,
        onSubmit: PropTypes.func,
        value: PropTypes.string,
        children: PropTypes.any,
        autoComplete: PropTypes.string,
    };
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }
    handleChange(e) {
        // second argument convenient when we want to
        // map value to the key later in the component state
        this.props.onInputChange(e.target.value, this.props.name);
    }
    handleKeyPress(e) {
        if (e.key === 'Enter') {
            this.props.onSubmit();
        }
    }
    render() {
        let {
            classNameWrap,
            label,
            name,
            type,
            placeholder,
            className,
            value,
            disabled,
            autoComplete,
            children,
        } = this.props;

        return (
            <div className={`form-input ${classNameWrap || ''}`}>
                {label ?
                <label htmlFor={name}>{label}</label> : null}
                <input name={name}
                 type={type}
                 onChange={this.handleChange}
                 onKeyPress={this.handleKeyPress}
                 placeholder={placeholder || ''}
                 className={className || ''}
                 value = {value}
                 disabled={disabled}
                 autoComplete={autoComplete}
                 />
                 {children ? children : null}
            </div>
        );
    }
}
export default Input;
