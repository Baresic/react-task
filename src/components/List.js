import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../scss/List.css';

class List extends Component {
    static propTypes = {
        names: PropTypes.any,
        removeSubscriber: PropTypes.func,
    };

    constructor(props){
        super(props);
        this.state = {}
    }
    removeItem(num){
        this.props.removeSubscriber(num);
    }
    render() {
        let {
            names
        } = this.props;

        const listItems = names.map((person, index) =>
            <li key={index}>
                <div>Name: {person.name}</div>
                <div> Email: {person.email}</div>
                <span className="close" onClick={() => this.removeItem(index)}>X</span>
            </li>
        );
        return (
            <ul className="List">
                {names ? listItems : null}
            </ul>
        );
    }
}

export default List;