import React, { Component } from 'react';
import PropTypes from 'prop-types';
import '../scss/Button.css';

class Button extends Component {
  static propTypes = {
    showInputView: PropTypes.func,
  };

  constructor(props){
    super(props);
    this.state = {
      view: true,
    }
    this.onButtonClick = this.onButtonClick.bind(this);
  }

  onButtonClick() {
    this.props.showInputView(this.state.view);
  }

  render() {
    return (
      <div className="Button">
       <button className="start" type="button" onClick={this.onButtonClick}>Click Me!</button>
      </div>
    );
  }
}

export default Button;