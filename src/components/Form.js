import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import Input from './Input';
import EmailValidator from '../utils/EmailValidator';
import '../scss/Form.css';

class Form extends Component {
  static propTypes = {
    addNewSubscriber: PropTypes.func,
    showButtonView: PropTypes.func,
  };

  constructor(props){
    super(props);
    this.state = {
      name: '',
      email: '',
      error: false,
      emailError: false,
      view: false,
    }

    this.onValidateEmail = this.onValidateEmail.bind(this);
    this.onNameInput = this.onNameInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.onEmptyInput = this.onEmptyInput.bind(this);
    this.validator = new EmailValidator();
  }

  onNameInput(value, key){
    this.setState({
      [key]: value,
    });
    if (value.length !== 0 && value.length < 5) {
      this.setState({
        error: true,
      });
    } else {
      this.setState({
        error: false,
      });
    }
  }

  onValidateEmail(value, key) {
    this.setState({
        [key]: value,
    });

    let email = this.validator.validate(value);
    if (email || value.length === 0) {
        try {
            this.setState({
              emailError: false,
            });
        } catch (e) {
            console.log(e);
        }
    } else {
        this.setState({
          emailError: true,
        });
    }
  }

  createPassObj() {
    let state = this.state;
    return {
        name: state.name,
        email: state.email,
    };
  }

  onEmptyInput() {
    this.setState({
      name: '',
      email: '',
    });
  }

  handleSubmit() {
    let state = this.state;
    let props = this.props;
    if(!state.error && !state.emailError) {
      props.addNewSubscriber(this.createPassObj());
      this.onEmptyInput();
      props.showButtonView(state.view);
    }
  }

  render() {
    let {
      name,
      email,
      error,
      emailError,
    } = this.state;

    let errorClass = classNames({
        'text-input': true,
        'error': error,
    });
    let emailErrorClass = classNames({
        'text-input': true,
        'error': emailError,
    });
    let disabledClass = classNames({
      'but': true,
      'disabled': !!(name === '' && email === ''),
    });

    return (
      <div className="Form">
        <form>
            <Input
            label="Name"
            onInputChange={this.onNameInput}
            type="text"
            name="name"
            value={name}
            className={errorClass}
            autoComplete="off"
            />
            {(error ? <span className="error">Min 5 char is required</span> : null)}
            <br/>
            <Input
            label="Email"
            onInputChange={this.onValidateEmail}
            type="email"
            name="email"
            value={email}
            className={emailErrorClass}
            autoComplete="off"
            />
            {(emailError ? <span className="error">Email not valid</span> : null)}
            <button className={disabledClass} type="button" onClick={this.handleSubmit}>Save</button>
        </form>
      </div>
    );
  }
}

export default Form;