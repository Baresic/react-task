import { InvalidEmailFormat } from './constants';

export default class EmailValidator {
    /**
     * validates email content and returns true if validation is successful
     * @throws
     * InvalidEmailFormat,
     * @param {*} email validation
     */
    validate(email) {
        let regex = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/;

        if (email === '') {
            return false;
        }
        if (email) {
            return regex.test(email);
        }
        throw InvalidEmailFormat;
    }
}