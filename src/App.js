import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import Form from './components/Form';
import Button from './components/Button';
import List from './components/List';
import logo from './logo.svg';
import './scss/App.css';

import { addNewSubscriber, removeSubscriber, showButtonView, showInputView  } from './actions/subscriberActions';

class App extends Component {
  static propTypes = {
    subscribers: PropTypes.any,
    addNewSubscriber: PropTypes.func,
    removeSubscriber: PropTypes.func,
    showButtonView: PropTypes.func,
    showInputView: PropTypes.func,
    showing: PropTypes.bool,
  };

  constructor(props){
    super(props);
    this.state = {
      subscribers: this.props.subscribers,
      showing: this.props.showing,
    }
  }

  componentWillReceiveProps(nextProps){
    if(this.props.subscribers !== nextProps.subscribers){
        this.setState({
          subscribers: nextProps.subscribers,
        });
    }
    if(this.props.showing !== nextProps.showing){
      this.setState({
        showing: nextProps.showing,
      });
    }
  }

  render() {
    let {
      addNewSubscriber,
      removeSubscriber,
      showButtonView,
      showInputView,
    } = this.props;

    let {
      subscribers,
      showing,
    } = this.state;

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to Test App :)</h1>
        </header>
        <div className="App-content">
          <div className="App-top">
            {(!showing) ?
            <Button showInputView={showInputView}/> :
            <Form addNewSubscriber={addNewSubscriber} showButtonView={showButtonView}/>
            }
          </div>
          <div className="App-list">
            <List
            names={subscribers}
            removeSubscriber={removeSubscriber}
            />
         </div>
         </div>
      </div>
    );
  }
}

const mapStateToProps = ({subscriberReducer}) => {
  return {
      subscribers: subscriberReducer.subscriber,
      showing: subscriberReducer.showing,
  };
};

const mapDispatchToProps = {
  addNewSubscriber,
  removeSubscriber,
  showInputView,
  showButtonView,
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
