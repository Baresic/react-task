import actionTypes from './actionTypes';

/**
 * Subsriber actions
 *
 */
export const addNewSubscriber = (payload = {}) => {
    return { type: actionTypes.ADD_NEW_SUBSCRIBER, payload };
};

export const removeSubscriber = (payload = {}) => {
    return { type: actionTypes.REMOVE_SUBSCRIBER, payload };
};

export const showButtonView = (payload = {}) => {
    return { type: actionTypes.SHOW_BUTTON_VIEW, payload };
};

export const showInputView = (payload = {}) => {
    return { type: actionTypes.SHOW_INPUT_VIEW, payload };
};