import keyMirror from 'keymirror';

let actionTypes = keyMirror({
    ADD_NEW_SUBSCRIBER: null,
    REMOVE_SUBSCRIBER: null,
    SHOW_BUTTON_VIEW: null,
    SHOW_INPUT_VIEW: null,
});

export default actionTypes;
